package pl.sdacademy;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic() //autentykacja typu basic
                .and()              //bo zwraca nam obiekt do konfiguracji i musimy wrócić do http konfigorwać kolejne rzeczy
                .authorizeRequests()
                .antMatchers("/some-secured-path")
                .authenticated()
                .antMatchers("/**")
                .permitAll();
    }
    // user1:pass1

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user1")
                .password("pass1")
                .roles("USER")
                .and()
                .withUser("admin1")
                .password("pass1")
                .roles("ADMIN");
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
