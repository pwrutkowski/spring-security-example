package pl.sdacademy;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

    @GetMapping("/unsecured-path")
    public void example1() {
    }

    @GetMapping("/some-secured-path")
    public void example2() {

    }


}
